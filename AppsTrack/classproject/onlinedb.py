import click
import openpyxl
import os
import sys
import django
from openpyxl import Workbook
from bs4 import BeautifulSoup

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'classproject.settings')
django.setup()
from onlineapp.models import *


@click.group()
def onlinedb():
    pass


@onlinedb.command()
def fromexcelcollege():
    wb = openpyxl.load_workbook('students.xlsx')
    sheet = wb['Colleges']
    no = 0
    for row in sheet.rows:
        pass
        no += 1
        if no != 1:
            c1 = College(name=row[0].value, location=row[2].value, acronym=row[1].value, contact=row[3].value)
            c1.save()


@onlinedb.command()
def fromexcelstudent():
    wb = openpyxl.load_workbook('students.xlsx')
    sheet = wb['Current']
    no = 0
    for row in sheet.rows:
        pass
        no+=1
        if no != 1:
            c1 = Student(name=row[0].value, email=row[2].value, db_folder=row[3].value, dropped_out=False,
                         college=College.objects.get(acronym=row[1].value))
            print(c1.name)
            c1.save()


@onlinedb.command()
@click.argument('file',nargs=1)
def fromhtml(file):
    soup=BeautifulSoup(open(file),'html.parser')
    rows=soup.find_all('tr')
    for x in range(1,len(rows)):
        row=rows[x].find_all('td')
        c1=MockTest1(problem1=int(row[2].text),problem2=int(row[3].text),problem3=int(row[4].text),problem4=int(row[5].text),total=int(row[6].text),student=Student.objects.get(db_folder=row[1].text.split('_')[2]))
        c1.save()

if __name__ == "__main__":
    onlinedb()
