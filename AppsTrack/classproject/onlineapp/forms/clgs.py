from onlineapp.models import *
from django import forms
from rest_framework import serializers


class ApiCollegeSerSingle(serializers.ModelSerializer):
    class Meta:
        model = College
        fields = ('id', 'name', 'acronym', 'contact', 'location')


class ApiCollegeSer(serializers.ModelSerializer):
    class Meta:
        model = College
        fields = ('id', 'name', 'acronym')


class ApiStudentSer(serializers.ModelSerializer):
    class Meta:
        model = Student
        fields = ('name', 'email', 'db_folder', 'dropped_out','college_id')

class ApiMOckSer(serializers.ModelSerializer):
    class Meta:
        model=MockTest1
        fields=('problem1','problem2','problem3','problem4','total')

class AddCollegeForm(forms.ModelForm):
    class Meta:
        model = College
        fields = "__all__"


class AddStudentForm(forms.ModelForm):
    class Meta:
        model = Student
        exclude = ['id', 'dob', 'college_id', 'college']


class AddMockForm(forms.ModelForm):
    class Meta:
        model = MockTest1
        exclude = ['id', 'total', 'student_id', 'student']


class LoginForm(forms.Form):
    Username = forms.CharField(
        required=True,
        widget=forms.TextInput(attrs={'class': 'input', 'placeholder': 'Enter User Name'})
    )
    Password = forms.CharField(
        required=True,
        widget=forms.PasswordInput(attrs={'class': 'input', 'placeholder': 'Enter Password'})
    )


class SignupForm(forms.Form):
    first_name = forms.CharField(
        required=True,
        widget=forms.TextInput(attrs={'class': 'input', 'label': 'Firstname', 'placeholder': 'Enter First Name'})
    )
    last_name = forms.CharField(
        required=True,
        widget=forms.TextInput(attrs={'class': 'input', 'placeholder': 'Enter Last Name'})
    )
    username = forms.CharField(
        required=True,
        widget=forms.TextInput(attrs={'class': 'input', 'placeholder': 'Enter User Name'})
    )
    password = forms.CharField(
        required=True,
        widget=forms.PasswordInput(attrs={'class': 'input', 'placeholder': 'Enter Password'})
    )
