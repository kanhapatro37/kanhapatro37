# Generated by Django 2.2.1 on 2019-06-07 06:06

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('onlineapp', '0004_mocktest1_teacher'),
    ]

    operations = [
        migrations.AlterField(
            model_name='student',
            name='email',
            field=models.CharField(max_length=100),
        ),
    ]
