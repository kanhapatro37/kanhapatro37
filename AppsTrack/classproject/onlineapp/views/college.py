from django.http import HttpResponse
from django.views import View
from rest_framework import status
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.decorators import api_view, permission_classes, authentication_classes
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.authentication import SessionAuthentication, BasicAuthentication,TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from onlineapp.models import *
from django.shortcuts import render, redirect
from onlineapp.forms.clgs import *
from django.urls import resolve
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.contrib.auth.models import User
from django.contrib.auth.mixins import LoginRequiredMixin
from rest_framework.authtoken.models import Token

def testview(request):
    return HttpResponse("<body><h1>my first response</h1></body>")

class CustomAuthToken(ObtainAuthToken):

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data,
                                           context={'request': request})
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, created = Token.objects.get_or_create(user=user)
        return Response({
            'token': token.key,
            'username': user.username,
            'password': user.password
        })

@api_view(['GET', 'PUT', 'POST', 'DELETE'])
@authentication_classes((SessionAuthentication, BasicAuthentication,TokenAuthentication))
@permission_classes((IsAuthenticated,))
def apicollege(request, *args, **kwargs):
    if kwargs:
        name = College.objects.get(**kwargs)
        if request.method == 'GET':
            jsonobj = ApiCollegeSerSingle(name)
            return Response(jsonobj.data, status=status.HTTP_200_OK)

        if request.method == 'PUT':
            pyobj = ApiCollegeSer(name, data=request.data)
            if pyobj.is_valid():
                pyobj.save()
                return Response(pyobj.data, status=status.HTTP_201_CREATED)
        if request.method == 'DELETE':
            name.delete()
            return Response(status=status.HTTP_204_NO_CONTENT)

    else:
        if request.method == 'GET':
            name = College.objects.values('id', 'name', 'acronym')
            jsonobj = ApiCollegeSer(name, many=True)
            return Response(jsonobj.data, status=status.HTTP_200_OK)

        if request.method == 'POST':
            pyobj = ApiCollegeSer(data=request.data)
            if pyobj.is_valid():
                pyobj.save()
                return Response(pyobj.data, status=status.HTTP_201_CREATED)


# @api_view(['GET', 'POST'])
class Apistudent(APIView):
    def get(request, *args, **kwargs):
        if kwargs.get('spk'):
            name = Student.objects.get(pk=kwargs.get('spk'))
            jsonobj = ApiStudentSer(name)
            print(jsonobj)
            return Response(jsonobj.data, status=status.HTTP_200_OK)

        else:
            name = Student.objects.values('name', 'dob', 'email', 'db_folder', 'dropped_out').filter(
                college_id=kwargs.get('cpk'))
            jsonobj = ApiStudentSer(name, many=True)
            return Response(jsonobj.data, status=status.HTTP_200_OK)

    # def post(self, request, *args, **kwargs):
    #     request.data['college_id'] = kwargs.get('cpk')
    #     student = Student.objects.create(**request.data)
    #     student.college = College.objects.get(pk=kwargs.get('cpk'))
    #     student.save()
    #     return Response({'message': student.name + ' created'}, status=status.HTTP_201_CREATED)

        # pyobj = ApiStudentSer(data=)
        # pyobj.initial_data['college'] = College.objects.get(pk=kwargs.get('cpk'))
        # pyobj.initial_data['college'] = int(kwargs.get('cpk'))
        # if pyobj.is_valid():
        # pyobj.save()
        # return Response(pyobj.data, status=status.HTTP_201_CREATED)
    def delete(self,request,*args,**kwargs):
        stu=Student.objects.get(pk=kwargs.get('spk'))
        stu.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

    def post(self,request,*args,**kwargs):
        clg=College.objects.get(pk=kwargs.get('cpk'))
        s=Student(college=clg)
        studata=ApiStudentSer(s,data=request.data)
        if studata.is_valid():
            studata.save()
            # return Response(studata.data, status=status.HTTP_201_CREATED)

        t = sum([request.data["mocktest1"]["problem1"],request.data["mocktest1"]["problem2"], request.data["mocktest1"]["problem3"], request.data["mocktest1"]["problem4"]])
        m = MockTest1(student=s)
        # m.total=t
        request.data['mocktest1'].update({'total':t})
        mockdata=ApiMOckSer(m,data=request.data["mocktest1"])
        if mockdata.is_valid():
            # mockdata.data["total"] = sum(mockdata.data["problem1"], mockdata.data["problem2"], mockdata.data["problem3"],
            #                            mockdata.data["problem4"])
            mockdata.save()
        d1=studata.data
        d1.update({"mocktest1":mockdata.data})
        if studata.is_valid() and mockdata.is_valid():
            return Response(d1,status=status.HTTP_201_CREATED)

    def put(self,request,*args,**kwargs):
        clg=College.objects.get(pk=kwargs.get('cpk'))
        s=Student.objects.get(pk=kwargs.get('spk'))
        studata=ApiStudentSer(s,data=request.data)
        if studata.is_valid():
            studata.save()
            # return Response(studata.data, status=status.HTTP_201_CREATED)

        t = sum([request.data["mocktest1"]["problem1"],request.data["mocktest1"]["problem2"], request.data["mocktest1"]["problem3"], request.data["mocktest1"]["problem4"]])
        m = MockTest1.objects.get(student_id=kwargs.get('spk'))
        # m.total=t
        request.data['mocktest1'].update({'total':t})
        mockdata=ApiMOckSer(m,data=request.data["mocktest1"])
        if mockdata.is_valid():
            # mockdata.data["total"] = sum(mockdata.data["problem1"], mockdata.data["problem2"], mockdata.data["problem3"],
            #                            mockdata.data["problem4"])
            mockdata.save()
        d1=studata.data
        d1.update({"mocktest1":mockdata.data})
        if studata.is_valid() and mockdata.is_valid():
            return Response(d1,status=status.HTTP_201_CREATED)

def logout_user(request):
    logout(request)
    return redirect('login')


class collegeview(LoginRequiredMixin, View):
    login_url = 'login'

    def get(self, request, *args, **kwargs):
        if kwargs:
            clg = College.objects.get(pk=kwargs.get('id'))
            details = Student.objects.values('name', 'db_folder', 'mocktest1__total', 'id').filter(
                college__id=kwargs.get('id')).order_by('-mocktest1__total')
            permissions = request.user.get_all_permissions()
            # print(permissions)
            # details=list(clg.student_set.order_by("-mocktest1__total"))
            return render(request, "details.html",
                          {"details": details, "college": clg.name, "id": kwargs.get('id'), "permissions": permissions})
        else:
            name = College.objects.values('name', 'acronym', 'id')
            permissions = request.user.get_all_permissions()
            # print(permissions)
            return render(request, "clglinks.html", {"colleges": name, "permissions": permissions})


class studentview(View):
    def get(self, request, id):
        details = Student.objects.values('name', 'college__acronym', 'mocktest1__total').filter(college__id=id)
        return render(request, "details.html", {"details": details})


class AddCollege(View):
    def get(self, request, *args, **kwargs):
        form = AddCollegeForm()
        action = 'Add College'
        if kwargs:
            college = College.objects.get(pk=kwargs.get('id'))
            form = AddCollegeForm(instance=college)
            if resolve(request.path_info).url_name == 'editclg':
                action = 'Edit College'
            if resolve(request.path_info).url_name == 'delclg':
                action = 'Delete College'
        return render(request, 'addcollege.html', {"form": form, "action": action})

    def post(self, request, *args, **kwargs):

        if resolve(request.path_info).url_name == 'editclg':
            college = College.objects.get(pk=kwargs.get('id'))
            form = AddCollegeForm(request.POST, instance=college)
            if form.is_valid():
                form.save()
                return redirect('clglinks')
        if resolve(request.path_info).url_name == 'delclg':
            College.objects.get(pk=kwargs.get('id')).delete()
            return redirect('clglinks')

        form = AddCollegeForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('clglinks')
        else:
            return redirect('addclg')


class AddStudent(View):
    def get(self, request, *args, **kwargs):
        stuform = AddStudentForm()
        mockform = AddMockForm()
        action = 'Add Student'
        if resolve(request.path_info).url_name == 'editstu' or resolve(request.path_info).url_name == 'delstu':
            student = Student.objects.get(pk=kwargs.get('id'))
            stuform = AddStudentForm(instance=student)
            marks = MockTest1.objects.get(student_id=kwargs.get('id'))
            mockform = AddMockForm(instance=marks)
            if resolve(request.path_info).url_name == 'editstu':
                action = 'Edit Student'
            if resolve(request.path_info).url_name == 'delstu':
                action = 'Delete Student'
        return render(request, 'addstudent.html', {"stuform": stuform, "mockform": mockform, "action": action})

    def post(self, request, *args, **kwargs):
        if resolve(request.path_info).url_name == 'editstu':
            student = Student.objects.get(pk=kwargs.get('id'))
            stuform = AddStudentForm(request.POST, instance=student)
            marks = MockTest1.objects.get(student_id=kwargs.get('id'))
            mockform = AddMockForm(request.POST, instance=marks)
            stud1 = Student()
            if stuform.is_valid():
                stud1 = stuform.save(commit=False)
                stuform.save()
            stud1marks = MockTest1()
            if mockform.is_valid():
                stud1marks = mockform.save(commit=False)
            stud1marks.total = stud1marks.problem1 + stud1marks.problem2 + stud1marks.problem3 + stud1marks.problem4
            stud1marks.student = stud1
            stud1marks.save()
            return redirect('studet', int(Student.objects.get(pk=kwargs.get('id')).college_id))
        if resolve(request.path_info).url_name == 'delstu':
            id = Student.objects.get(pk=kwargs.get('id')).college_id
            Student.objects.get(pk=kwargs.get('id')).delete()
            # MockTest1.objects.get(student_id=kwargs.get('id')).delete()
            return redirect('studet', id)

        # student = Student.objects.get(pk=kwargs.get('id'))
        # student.college_id = kwargs['id']
        stuform = AddStudentForm(request.POST)
        mockform = AddMockForm(request.POST)
        stud1 = Student()
        if stuform.is_valid():
            stud1 = stuform.save(commit=False)
        stud1.college = College.objects.get(pk=kwargs.get('id'))
        stud1.save()
        stud1marks = MockTest1()
        if mockform.is_valid():
            stud1marks = mockform.save(commit=False)
        stud1marks.total = stud1marks.problem1 + stud1marks.problem2 + stud1marks.problem3 + stud1marks.problem4
        stud1marks.student = stud1
        stud1marks.save()
        return redirect('studet', kwargs.get('id'))


class Login(View):
    def get(self, request):
        form = LoginForm()
        action1 = 'Login'
        action2 = 'Signup'
        return render(request, "login.html", {'form': form, 'action1': action1, 'action2': action2})

    def post(self, request, *args, **kwargs):
        form = LoginForm(request.POST)
        if form.is_valid():
            user = authenticate(request, username=form.cleaned_data['Username'], password=form.cleaned_data['Password'])
            if user is not None:
                login(request, user)
                return redirect('clglinks')
            else:
                messages.error(request, 'Invalid Credentials')
                return redirect('login')


class Signup(View):
    def get(self, request):
        form = SignupForm()
        action1 = 'Signup'
        action2 = 'Login'
        return render(request, "login.html", {'form': form, 'action1': action1, 'action2': action2})

    def post(self, request, *args, **kwargs):
        form = SignupForm(request.POST)
        if form.is_valid():
            user = User.objects.create_user(**form.cleaned_data)
            user.save()
            login(request, user)
            return redirect('clglinks')
