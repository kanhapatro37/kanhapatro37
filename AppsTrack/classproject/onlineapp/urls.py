from django.contrib import admin
from django.urls import path
from django.conf import settings
from django.conf.urls import include, url  # For django versions before 2.0
from django.urls import include, path  # For django versions from 2.0 and up
from onlineapp.views import college


urlpatterns = [
    #path('hello/',views.hello_world),
    #path('getclg/',views.get_college),
    #path('getclglist/',views.get_college_list),
    #path('sample/',views.sample),
    #path('sample2/',views.sample2),
    #path('clglinks/',views.clglinks),
    #path('students/<int:id>',college.studentview.as_view()),
    path('apicolleges/',college.apicollege,name="apiclgurl"),
    path('apicolleges/<int:id>',college.apicollege,name="apiclgurlsingle"),

    path('apicolleges/<int:cpk>/students',college.Apistudent.as_view(),name="apistudents"),
    path('apicolleges/<int:cpk>/students/<int:spk>',college.Apistudent.as_view(),name="apistudentsSingle"),
    path('api-token-auth/', college.CustomAuthToken.as_view(),name="authlogin"),

    path('colleges/<int:id>',college.collegeview.as_view(),name="studet"),
    path('colleges/',college.collegeview.as_view(),name="clglinks"),

    path('addcollege/',college.AddCollege.as_view(),name="addclg"),
    path('addcollege/<int:id>/edit',college.AddCollege.as_view(),name="editclg"),
    path('addcollege/<int:id>/delete',college.AddCollege.as_view(),name="delclg"),

    path('addstudent/<int:id>',college.AddStudent.as_view(),name="addstu"),
    path('addstudent/<int:id>/edit',college.AddStudent.as_view(),name="editstu"),
    path('addstudent/<int:id>/delete',college.AddStudent.as_view(),name="delstu"),

    path('Login/',college.Login.as_view(),name="login"),
    path('Signup/',college.Signup.as_view(),name="signup"),
    path('logout/',college.logout_user,name="logout"),

    path('testurl',college.testview,name="test")

]