from django.contrib import admin
from django.urls import path
from django.conf import settings
from django.conf.urls import include, url  # For django versions before 2.0
from django.urls import include, path  # For django versions from 2.0 and up
from iplclone.views import ipl

urlpatterns=[
    path('home/',ipl.home.as_view(),name="home"),
    path('season_details/<int:year>',ipl.Season_Details.as_view(),name="Season_Details"),
    path('matchDetails/<int:id>', ipl.DisplayMatchDetails.as_view(), name='display_match_details'),
    path('logout/', ipl.logout_user, name='logout'),
    path('login/', ipl.Login.as_view(), name='login'),
    path('Signup/', ipl.SignUp.as_view(), name='register'),
    path('teamhome/<int:id>', ipl.TeamHomePage.as_view(), name='displayTeamHome'),

    # path('homere/',ipl.homere.as_view(),name="homere"),
]