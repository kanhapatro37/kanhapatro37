from django.db.models import Count
from django.http import HttpResponse
from django.views import View
from rest_framework import status
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.decorators import api_view, permission_classes, authentication_classes
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.authentication import SessionAuthentication, BasicAuthentication,TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from iplclone.models import *
from django.shortcuts import render, redirect
from django.urls import resolve
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.contrib.auth.models import User
from django.contrib.auth.mixins import LoginRequiredMixin
from rest_framework.authtoken.models import Token
from django.core.paginator import Paginator
from iplclone.forms.clgs import *

def logout_user(request):
    logout(request)
    return redirect('login')


def getTeamStats(team):
    seasonStats = []
    for year in range(2008,2020):
        temp = pointsTable(year)
        for row in temp:
            if(row[1]==team):
                row.pop(1)
                row.insert(0,year)
                seasonStats.append(row)
    return seasonStats

class TeamHomePage(View):
    def get(self,request,*args,**kwargs):
        teams = {1: 'Royal Challengers Bangalore', 2: 'Mumbai Indians', 3: 'Sunrisers Hyderabad', 4: 'Rajasthan Royals',
                 5: 'Delhi Capitals',
                 6: 'Kings XI Punjab', 7: 'Chennai Super Kings', 8: 'Gujarat Lions', 9: 'Rising Pune Supergiant',
                 10: 'Pune Warriors', 11: 'Kolkata Knight Riders',
                 12: 'Kochi Tuskers Kerala', 13: 'Deccan Chargers', 14: 'Delhi Daredevils'}
        seasonStats = getTeamStats(teams[kwargs['id']])
        return render(request,'TeamPage.html',{'stats':seasonStats,'team':teams[kwargs['id']]})

def pointsTable(year):
    table = []
    total_teams = Matches.objects.filter(year=year).values('team1').distinct().count()
    total_matches = (total_teams-1) * 2
    total_matches_upto_playoffs = (total_matches*total_teams)//2
    temp = Matches.objects.filter(year = year).values_list('winner').annotate(won = Count('winner')).order_by('-won')
    for i in temp:
        a,b = i
        if(a is not None):
            table.append([a,b])
    for i in range(total_teams):
        table[i].insert(1,total_matches)
        table[i].append(total_matches - table[i][2])
        table[i].insert(0,i+1)
    return table

class home(View):
    def get(self,request):
        matches=Matches.objects.filter(year=2019)
        paginator=Paginator(matches,8)
        page = request.GET.get('page')
        matches = paginator.get_page(page)
        years=Matches.objects.values("year").distinct().order_by('-year')
        if (request.user == request.user.is_anonymous):
            print('is fu***ing anonymous')
            user = 0
        else:
            user = request.user
        return render(request,"home.html",context={
            "matches":matches,
            "years":years,
            'user': user,
            'pt':pointsTable(2019)
        })

class Season_Details(View):
    def get(self,request,**kwargs):
        matches = Matches.objects.filter(year=kwargs.get('year'))
        season=kwargs.get('year')-2007
        years=Matches.objects.values("year").distinct().order_by('-year')
        paginator = Paginator(matches, 8)
        page = request.GET.get('page')
        matches = paginator.get_page(page)
        return render(request, "season_details.html", context={
            "matches": matches,
            "season":season,
            "years":years,
            'pt':pointsTable(kwargs.get('year'))
        })
class DisplayMatchDetails(LoginRequiredMixin,View):
    login_url = '/login/'
    def get(self,request,*args,**kwargs):
        if(kwargs):
            # match = Matches.objects.filter(matchid = kwargs['id']).values_list("team1", "team2", "venue", "toss", "toss_decision","winner", "mom","matchid")
            match = Matches.objects.get(match_id = kwargs['id'])
            details = Deliveries.objects.filter(match_id = kwargs['id'])
            # print('is fu***ing anonymous',request.user.is_anonymous)
            if(request.user==request.user.is_anonymous):
                user = 0
                print('is fu***ing anonymous')
            else:
                user = request.user
            return render(request,"match_details.html",{'match' : match,'details':details,'user':user})

class Login(View):
    def get(self, request):
        form = LoginForm()
        action1 = 'Login'
        action2 = 'Signup'
        return render(request, "login.html", {'form': form, 'action1': action1, 'action2': action2})

    def post(self, request, *args, **kwargs):
        form = LoginForm(request.POST)
        if form.is_valid():
            user = authenticate(request, username=form.cleaned_data['Username'],
                                password=form.cleaned_data['Password'])
            if user is not None:
                login(request, user)
                return redirect('home')
            else:
                messages.error(request, 'Invalid Credentials')
                return redirect('login')

class SignUp(View):
    def get(self, request):
        form = SignupForm()
        action1 = 'Signup'
        action2 = 'Login'
        return render(request, "login.html", {'form': form, 'action1': action1, 'action2': action2})

    def post(self, request, *args, **kwargs):
        form = SignupForm(request.POST)
        if form.is_valid():
            user = User.objects.create_user(**form.cleaned_data)
            user.save()
            login(request, user)
            return redirect('home')
