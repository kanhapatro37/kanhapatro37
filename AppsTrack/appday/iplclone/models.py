from django.db import models

# Create your models here.
class Matches(models.Model):
    match_id=models.IntegerField(primary_key=True)
    year=models.IntegerField(null=True)
    city=models.CharField(max_length=60,null=True)
    date=models.DateField(null=True)
    team1=models.CharField(max_length=60,null=True)
    team2=models.CharField(max_length=60,null=True)
    toss_win=models.CharField(max_length=60,null=True)
    toss_decision=models.CharField(max_length=60,null=True)
    result_type=models.CharField(max_length=60,null=True)
    d_l=models.BooleanField(null=True)
    winner=models.CharField(max_length=60,null=True)
    win_by_runs=models.IntegerField(default=0,null=True)
    win_by_wkts=models.IntegerField(default=0,null=True)
    pom=models.CharField(max_length=60,null=True)
    venue=models.CharField(max_length=60,null=True)
    umpire1=models.CharField(max_length=60,null=True)
    umpire2=models.CharField(max_length=60,null=True)
    umpire3=models.CharField(max_length=60,null=True)

    def __str__(self):
        return self.team1+' vs '+self.team2

class Deliveries(models.Model):
    match_id=models.IntegerField(null=True)
    inning=models.IntegerField(null=True)
    batting_team=models.CharField(max_length=60,null=True)
    bowling_team=models.CharField(max_length=60,null=True)
    ovr=models.IntegerField(default=0,null=True)
    ball=models.IntegerField(default=0,null=True)
    batsman=models.CharField(max_length=60,null=True)
    non_striker=models.CharField(max_length=60,null=True)
    bowler=models.CharField(max_length=60,null=True)
    is_super_over=models.BooleanField(null=True)
    wide_runs=models.IntegerField(default=0,null=True)
    bye_runs=models.IntegerField(default=0,null=True)
    legbye_runs=models.IntegerField(default=0,null=True)
    noball_runs=models.IntegerField(default=0,null=True)
    penalty_runs=models.IntegerField(default=0,null=True)
    batsman_runs=models.IntegerField(default=0,null=True)
    extra_runs=models.IntegerField(default=0,null=True)
    total_runs=models.IntegerField(default=0,null=True)
    player_dismissed=models.CharField(max_length=60,null=True)
    dismissal_kind=models.CharField(max_length=60,null=True)
    fielder=models.CharField(max_length=60,null=True)

    def __str__(self):
        return self.batting_team+' vs '+self.bowling_team+"  over:"+str(self.ovr)+"  ball:"+str(self.ball)