# Generated by Django 2.2.1 on 2019-06-17 07:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('iplclone', '0006_auto_20190617_1232'),
    ]

    operations = [
        migrations.AlterField(
            model_name='matches',
            name='city',
            field=models.CharField(max_length=60, null=True),
        ),
        migrations.AlterField(
            model_name='matches',
            name='d_l',
            field=models.BooleanField(null=True),
        ),
        migrations.AlterField(
            model_name='matches',
            name='date',
            field=models.DateField(null=True),
        ),
        migrations.AlterField(
            model_name='matches',
            name='pom',
            field=models.CharField(max_length=60, null=True),
        ),
        migrations.AlterField(
            model_name='matches',
            name='result_type',
            field=models.CharField(max_length=60, null=True),
        ),
        migrations.AlterField(
            model_name='matches',
            name='team1',
            field=models.CharField(max_length=60, null=True),
        ),
        migrations.AlterField(
            model_name='matches',
            name='team2',
            field=models.CharField(max_length=60, null=True),
        ),
        migrations.AlterField(
            model_name='matches',
            name='toss_decision',
            field=models.CharField(max_length=60, null=True),
        ),
        migrations.AlterField(
            model_name='matches',
            name='toss_win',
            field=models.CharField(max_length=60, null=True),
        ),
        migrations.AlterField(
            model_name='matches',
            name='umpire1',
            field=models.CharField(max_length=60, null=True),
        ),
        migrations.AlterField(
            model_name='matches',
            name='umpire2',
            field=models.CharField(max_length=60, null=True),
        ),
        migrations.AlterField(
            model_name='matches',
            name='umpire3',
            field=models.CharField(max_length=60, null=True),
        ),
        migrations.AlterField(
            model_name='matches',
            name='venue',
            field=models.CharField(max_length=60, null=True),
        ),
        migrations.AlterField(
            model_name='matches',
            name='winner',
            field=models.CharField(max_length=60, null=True),
        ),
        migrations.AlterField(
            model_name='matches',
            name='year',
            field=models.IntegerField(null=True),
        ),
    ]
