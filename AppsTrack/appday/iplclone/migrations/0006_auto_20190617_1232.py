# Generated by Django 2.2.1 on 2019-06-17 07:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('iplclone', '0005_auto_20190617_1226'),
    ]

    operations = [
        migrations.AlterField(
            model_name='matches',
            name='win_by_runs',
            field=models.IntegerField(default=0, null=True),
        ),
        migrations.AlterField(
            model_name='matches',
            name='win_by_wkts',
            field=models.IntegerField(default=0, null=True),
        ),
        migrations.AlterField(
            model_name='matches',
            name='winner',
            field=models.CharField(max_length=30, null=True),
        ),
    ]
