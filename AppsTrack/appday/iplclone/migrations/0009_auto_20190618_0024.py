# Generated by Django 2.2.1 on 2019-06-17 18:54

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('iplclone', '0008_deliveries'),
    ]

    operations = [
        migrations.RenameField(
            model_name='deliveries',
            old_name='over',
            new_name='ovr',
        ),
    ]
