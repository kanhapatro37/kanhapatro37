import click
import openpyxl
import csv
import os
import sys
import django
from openpyxl import Workbook
from bs4 import BeautifulSoup
from django.db import connection

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'appday.settings')
django.setup()
from iplclone.models import *

@click.group()
def onlinedb():
    pass

@onlinedb.command()
def fromexcelmatches():
    wb = openpyxl.load_workbook('matches.xlsx')
    sheet=wb['matches']
    n=0
    for row in sheet.rows:
        if n==0:
            n+=1
            pass
        elif row[0].value:
            m=Matches(match_id=int(row[0].value),
                      year=int(row[1].value),
                      city=row[2].value,
                      date=row[3].value,
                      team1=row[4].value,
                      team2=row[5].value,
                      toss_win=row[6].value,
                      toss_decision=row[7].value,
                      result_type=row[8].value,
                      d_l=bool(row[9].value),
                      winner=row[10].value,
                      win_by_runs=int(row[11].value),
                      win_by_wkts=int(row[12].value),
                      pom=row[13].value,
                      venue=row[14].value,
                      umpire1=row[15].value,
                      umpire2=row[16].value,
                      umpire3=row[17].value
                      )
            m.save()


@onlinedb.command()
def fromexceldeliveries():
    with open(r'deliveries1.csv') as f:
        data = [tuple(line) for line in csv.reader(f)]
    cursor = connection.cursor()
    query = "INSERT INTO iplclone_deliveries (match_id , inning , batting_team , bowling_team , ovr , ball , batsman , non_striker , bowler , is_super_over , wide_runs , bye_runs , legbye_runs , noball_runs , penalty_runs , batsman_runs , extra_runs , total_runs , player_dismissed , dismissal_kind , fielder) VALUES (%s, %s, %s,%s, %s, %s,%s, %s, %s,%s, %s, %s,%s, %s, %s,%s, %s, %s,%s, %s, %s);"
    data = tuple(data[1:])
    #print(data[1])
    #print(tuple(data))
    cursor.executemany(query,data)
    #print(data[0][0])
    print(len(data))


if __name__ == "__main__":
    onlinedb()
#
# f=open("deliveries1.csv")
# file=csv.DictReader(f)
# for c in file:
#     delobj=Deliveries(**c)
#     delobj.save()
#
# f.close()